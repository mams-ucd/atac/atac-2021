# Maze Agents

You will need:

- Java 11
- [Apache Maven](https://maven.apache.org/download.cgi/)

# Running the Code

- Download the [maze server](https://github.com/all-agents-challenge/maze-server)
- Either: send a curl request to the agents for the desired room (see section "Testing" below)
- Or: download and run the [Query Manager server](https://gitlab.com/mams-ucd/atac-query-manager)

- In [src/main/astra/PathFinder.astra](https://gitlab.com/mams-ucd/atac-2021/-/blob/main/src/main/astra/PathFinder.astra), modify the `url("start","http://localhost:1337/#it")` initial belief to point to the start uri of the deployed maze server
- Compile and run the agents using `mvn`

# Navigating the maze

The main agent (Navi) registers with the Query Manager if it is running. It listens to incoming requests to `/main/query`.

The request body should comprise of the destination room and location of the requestor (for feedback). See examples in the section "Testing" below.

The main agent queries it's beliefs to see if it already knows the way (`path_to(string roomName, Path p)`).

If not, it queries it's beliefs regarding any subagents, and if it is aware of any, messages them (using a local MQ service) with a request for information. 

If unsucessful, it creates a new subagent. If the main agent has any beliefs relating to the maze layout, it sends the information to the subagent. It then initialises the subagent's goal, sending the required room as a parameter. If running, the subagent is registered with the Query Manager.

The sub-agents use Reinforcement Learning to find the goal rooms. They also share general schematic knowledge with the main agent. 

## Subagents (PathFinder.astra)

- Have an initial belief about the number of iterations with which to explire the maze (`max_iterations(int)`).
- Exploring the maze involves:
 - Randomly picking the next room (exploration) or searching existing knowledge about the best room to go into next (exploitation).
 - Updating local location information at each step
 - Updating the value of the chosen rooms at each step
 - Communicating the result to the main agent. Depending on the number of iterations it may not be the shortest path
 - It then sends information on all known locations to the main agent so the next agent created has an idea of the layout

## Knowledge

- Individual agents "look around" the URLs and get information about the adjoining rooms, and 
 assign a value according to the Q Learning algorithm, over successive training iterations
- At the end of training, the path is shared (array list of locations)
- The locations are cleared of values, and value-less path of all locations visited is shared
 (as the values correspond to the goal room, which will be different for different agents)

## Testing 

Either: run the [Query Manager server](https://gitlab.com/mams-ucd/atac-query-manager) and send requests via the browser, or use the curl requests below, replacing the IP address with the location of the agent.
Please note, you will need to copy the curl requests from the raw README.md file, as the rendered markdown hides the new line character (backslah).

1. Bedroom

curl -X POST \
 http://192.168.8.134:9000/Navi/query \
-H 'Content-Type: application/json' \
-H 'accept: text/plain' \
-d '{"room":"Bedroom"}'

The response should be (on Query Manager, and/or in the agent console):
``
Instructions to Bedroom:
From Entry go east into Formal Dining
Next, go south into Closet
Next, go west into Living Room
Next, go south into Dressing Room
Next, go south into Family Room
Next, go south into Kitchen
Next, go south into Formal Living Room
Next, go east into Bedroom 
``

2. Walk-In Closet 

This is east from the Bedroom, a new agent should be required to find it.

curl -X POST \
 http://192.168.8.134:9000/Navi/query  \
-H 'Content-Type: application/json' \
-H 'accept: text/plain' \
-d '{"room":"Walk-In Closet"}'


3. Formal Living Room 

This is before Bedroom, so either the Bedroom or Walk-In Closet subagents should send the subpath to the main agent.

curl -X POST \
 http://192.168.8.134:9000/Navi/query \
-H 'Content-Type: application/json' \
-H 'accept: text/plain' \
-d '{"room":"Formal Living Room"}'

4. Mud Room

The Mud Room is after the Bedroom and Walk-In Closet, so should require a new agent to find it:

curl -X POST \
 http://192.168.8.134:9000/Navi/query \
-H 'Content-Type: application/json' \
-H 'accept: text/plain' \
-d '{"room":"Mud Room"}'
