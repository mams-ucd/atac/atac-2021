package astra.navigation;

import java.util.HashMap;

/*
 * TODO: directions should come from schema and be set by reflection
 */
public class Location {

    String id;
    String label;
    HashMap<String, String> adjacentLocations = new HashMap<>();
    
    //score assigned "getting into" location, from parent.
    HashMap<String, Double> score = new HashMap<>(); //Key: "where came from", Value: score

    public void addAdjacentRoom(String direction, String locUrl) {
        adjacentLocations.put(direction, locUrl);
    }

    public Location(String id) {
        this.id = id;
    }

    public Location(String id, String label) {
        this.id = id;
        this.label = label;
    }

    public String getId() {
        return this.id;
    }
    
    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public HashMap<String, String> getAdjacentLocations() {
        return adjacentLocations;
    }

    public String getDirection(String nextRoom) {
        for (String direction: adjacentLocations.keySet()) {
            String room = adjacentLocations.get(direction);
            if (room.equals(nextRoom)) {
                return direction;
            }
        }
        return null;
    }

    public void flushScores() {
        score = new HashMap<>();
    }

    public double getScore(String parentId) {
        Double s = score.get(parentId);
        if (s == null) {
            return -1;
        }
        return (double)s;
    }
    
    public void addScore(String parentId, double dscore) {
        score.put(parentId, dscore);
    }

    @Override
    public String toString() {
        String retval = this.id;
        if (this.label!=null) retval += " " + this.label;

        if (!adjacentLocations.isEmpty()) {
            retval += ". Adjacent Rooms: ";
            for (String direction: adjacentLocations.keySet()) {
                retval += direction + ":" + adjacentLocations.get(direction) + ",";
            }
            retval = retval.substring(0, retval.length()-1);
        }
        return retval;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Location) {
            Location otherLoc = (Location)other;
            if (otherLoc.id.equals(id)) {
                return true;
            }
        }
        return false;
    }

    public boolean mapped() {
        return !label.isEmpty();
    }
}
