package astra.navigation;

import astra.core.Module.TERM;
import astra.navigation.Path.Pair;

import java.util.List;

import com.google.gson.Gson;

import astra.core.Module;

public class PathAccess extends Module {

    @TERM
    public String printPath(Path p) {
        return p.toString();
    }

    @TERM
    public String printDetailedPath(Path path) {
        List<Pair> pathList = path.getPath();
        if (pathList.size() < 1) {
            return "No information";
        }
        String destination = pathList.get(pathList.size()-1).getRoomName();
        String retval = "Instructions to " + destination + ":\n";
        retval += "From ";
        for (Pair p: pathList) {
            String incomingDirection = p.getIncomingDirection();
            if (incomingDirection!=null && !incomingDirection.isEmpty()) {
                retval += incomingDirection + " into ";
                retval += p.getRoomName();
                retval += "\n";
                retval += "Next, go ";
            } else {
                retval += p.getRoomName() + " go ";
            }
        }
        int intext = retval.lastIndexOf("\n");
        retval = retval.substring(0, intext);
        return retval;
    }

    @TERM
    public LocationStore augmentIntel(LocationStore existing, LocationStore newPath) {
        if (existing == null) {
            return newPath;
        } else {
            existing.add(newPath);
        }
        return existing;
    }
    
    @TERM
    public String getJSON(Path path) {
        Gson g = new Gson();
        String json = g.toJson(path.getPath());
        System.out.println(json);
        return json;
    }

}
