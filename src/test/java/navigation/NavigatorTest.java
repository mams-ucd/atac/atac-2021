package navigation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Set;

import org.junit.Test;

import astra.navigation.Location;
import astra.navigation.Navigator;

public class NavigatorTest {
    
    Navigator nav = new Navigator();

    public Location createLocation() {
        Location current = new Location("start");
        Location north = new Location("north");
        Location south = new Location("south");
        Location east = new Location("east");
        Location west = new Location("west");
        
        current.addAdjacentRoom("north", north.getId());
        north.addScore(current.getId(), 5);
        south.addScore(current.getId(), 2);
        current.addAdjacentRoom("south", south.getId());
        current.addAdjacentRoom("east", east.getId());
        current.addAdjacentRoom("west", west.getId());
        return current;
    }

    @Test
    public void testChooseNextLocationRandom() {
        Location current = createLocation();

        HashMap<String, Integer> counts = new HashMap();
        for (int i=0;i<1000;i++) {
            String result = nav.chooseNextLocationRandom(current);
            Integer c = counts.get(result);
            if (c == null) {
                counts.put(result, 0);
            } else {
                c = c+1;
                counts.put(result, c);
            }
        }

        Set<String> keys = counts.keySet();
        assertTrue(keys.contains("north"));
        int n = counts.get("north");
        System.out.println("North count: " + n);
        assertTrue(n > 225 && n < 275);
        
        assertTrue(keys.contains("east"));
        n = counts.get("east");
        System.out.println("east count: " + n);
        assertTrue(n > 225 && n < 275);
        
        assertTrue(keys.contains("west"));
        n = counts.get("west");
        System.out.println("west count: " + n);
        assertTrue(n > 225 && n < 275);
        
        assertTrue(keys.contains("south"));
        n = counts.get("south");
        System.out.println("south count: " + n);
        assertTrue(n > 225 && n < 275);
        
    }

    @Test
    public void testChooseNextLocation() {
       /* Location c = createLocation();
        Navigator nav = new Navigator();
        
        String nextLoc = nav.chooseNextLocation(c);
        assertEquals("north", nextLoc);*/
    }
}
