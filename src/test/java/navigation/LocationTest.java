package navigation;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import astra.navigation.Location;

public class LocationTest {
    
    @Test
    public void testToString() {
        Location loc = new Location("location0");
        loc.addAdjacentRoom("left", "location1");
        loc.addAdjacentRoom("right", "location2");

        String desc = loc.toString();
        assertEquals("location0. Adjacent Rooms: left:location1,right:location2", desc);
    }

    @Test
    public void testEquals() {
        Location loc = new Location("location0");
        loc.addAdjacentRoom("left", "location1");
        loc.addAdjacentRoom("right", "location2");

        Location loc1 = new Location("location0");
        loc.addAdjacentRoom("left", "location8");
        loc.addAdjacentRoom("right", "location9");

        assertEquals(loc, loc1);
    }
}
